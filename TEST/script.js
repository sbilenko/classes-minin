// class Team {
//     constructor(name, sport) {
//         this._name = name
//         this._sport = sport
//     }

//     get name() {
//         return this._name
//     }

//     set name(value) {
//         this._name = value
//     }

//     get sport() {
//         return this._sport
//     }

//     set sport(value) {
//         this._sport = value
//     }
// }

// const barcelona = new Team('Barcelona', 'football')
// barcelona.sport = 'basketball'
// console.log(barcelona)

// ---------------------------------------------------------------------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------------------------------- //

// class wildAnimals {
//     constructor(name, color) {
//         this.name = name
//         this.color = color
//     }

//     static type() {
//         console.log('These animals are wild.')
//     }

//     voice() {
//         console.log(`Im a ${this.name}`)
//     }
// }

// const bear = new wildAnimals('Greezly', 'grey')
// console.log(bear)
// console.log(bear.voice())

// class Dogs extends wildAnimals {
//     constructor(name, color, hasOwner, age) {
//         super(name, color)
//         this.hasOwner = hasOwner
//         this.age = age
//     }

//     static type() {
//         console.log('These animals are domestic.')
//     }

//     voice() {
//         super.voice()
//         console.log(`Hi, I am ${this.name}`)
//     }

//     get ageInfo() {
//         return this.age
//     }

//     set ageInfo(newAge) {
//         return this.age = newAge 
//     }
// }

// const dog = new Dogs('Baron', 'black', true, 10)
// console.log(dog)
// console.log(dog.voice())
// console.log(dog.ageInfo)

// ---------------------------------------------------------------------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------------------------------- //

// class Component {
//     constructor(selector) {
//         this.$el = document.querySelector(selector)
//     }

//     hide() {
//         this.$el.style.display = 'none'
//     }

//     show() {
//         this.$el.style.display = 'block'
//     }
// }

// class Square extends Component {
//     constructor(options) {
//         super(options.selector)

//         this.$el.style.width = this.$el.style.height = options.size + 'px'
//         this.$el.style.background = options.color
//     }
// }

// const square1 = new Square({
//     selector: '#square1',
//     size: 100,
//     color: 'red',
// })

// const square2 = new Square({
//     selector: '#square2',
//     size: 150,
//     color: 'blue', 
// })

// class Circle extends Square {
//     constructor(options) {
//         super(options)

//         this.$el.style.borderRadius = '50%'
//     }
// }

// const circle1 = new Circle({
//     selector: '#circle1',
//     size: 80,
//     color: 'green'
// })